{-
   File	       : $1
   Module      : AI.MDP
   Copyright   : Copyright (C) 2012 Oliver Stollmann
   License     : New BSD License (3-clause)

   Maintainer  : Oliver Stollmann <oliver@stollmann.net>
   Stability   : alpha
   Portability : portable

   <Module documentation>
-}

module AI.MDP (
     Prob
    ,Rwrd
    ,MDP(..)
    ,UpdatableMDP(..)
    ,BuildableMDP(..)
    ,stateNames
    ,actionNames
    ,stateIx
    ,actionIx
    ,stateIxFromName
    ,actionIxFromName
    ,tp'
    ,rw'
    ,updateTP'
    ,updateRW'
) where

import AI.MDP.Types
