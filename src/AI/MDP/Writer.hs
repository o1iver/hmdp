{-
   File	       : $1
   Module      : AI.MDP.Writer
   Copyright   : Copyright (C) 2012 Oliver Stollmann
   License     : New BSD License (3-clause)

   Maintainer  : Oliver Stollmann <oliver@stollmann.net>
   Stability   : alpha
   Portability : portable

   <Module documentation>
-}

module AI.MDP.Writer (
    toJSON
) where

import Text.JSON
import Text.JSON.Pretty

import AI.MDP.Types

toJSON :: MDP p s a => p s a -> String
toJSON = render . pp_js_object . toJSON'

toJSON' :: MDP p s a => p s a -> JSObject JSValue
toJSON' p = toJSObject [
             ("state_names"      ,showJSON $ stateNames  p)
            ,("action_names"     ,showJSON $ actionNames p)
            ,("tps"              ,showJSON $ tps         p)
            ,("rws"              ,showJSON $ rws         p)
            ]
