{-
   File	       : $1
   Module      : AI.MDP.Simulation
   Copyright   : Copyright (C) 2012 Oliver Stollmann
   License     : New BSD License (3-clause)

   Maintainer  : Oliver Stollmann <oliver@stollmann.net>
   Stability   : alpha
   Portability : portable

   <Module documentation>
-}

module AI.MDP.Probability where

import Data.List
import System.Random

type Distribution = [Double]

-- Given a random number generator and a distribution such as [0.2,0.4,0.4],
-- this function will return index n with probability dist[n]. It also returns
-- the next generator.
sample :: (RandomGen g) => g -> Distribution -> (Int,g)
sample rGen dist = case findIndex ((<=) randNum) probs of
    Just i -> (i,newGen)
    Nothing -> error "AI.MDP.Probability: sum of probabilities smaller than 1.0"
    where probs   = map (\i -> sum $ take i dist) [1..length dist]
          r       = random rGen
          randNum = fst r 
          newGen  = snd r





