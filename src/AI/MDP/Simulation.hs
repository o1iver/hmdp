{-
   File	       : $1
   Module      : AI.MDP.Simulation
   Copyright   : Copyright (C) 2012 Oliver Stollmann
   License     : New BSD License (3-clause)

   Maintainer  : Oliver Stollmann <oliver@stollmann.net>
   Stability   : alpha
   Portability : portable

   Simulation of MDPs.
-}

module AI.MDP.Simulation (
     simulateStep
    ,simulateStep_
    ,simulatePolicy
    ,simulatePolicy_
    ,simulateRandomPolicy
    ,simulateRandomPolicy_
) where

import AI.MDP.Types
import AI.MDP.Probability

import System.Random

-- Simulate a single episode using a random generator, an MDP, an action and a
-- start state. This function get the transition probabilities for the given
-- state and action and then samples against that distribution using the random
-- number generator. It returns the transitioned-to state and a new random 
-- number generator.
simulateStep :: (RandomGen g,MDP p s a) => g -> p s a -> a -> s -> (s,g)
simulateStep rGen mdp action_ state = (states mdp !! fst r,snd r)
    where _tps = tpAS mdp action_ state
          r = sample rGen _tps

-- Same as simulateStep, except that it does not return the new number
-- generator, only the transitioned-to state.
simulateStep_ :: (RandomGen g,MDP p s a) => g -> p s a -> a -> s -> s
simulateStep_ g m a s =  fst $ simulateStep g m a s

-- Simulate a policy using the given random number simulator, the policy, the
-- starting state and the required number of episodes. The function returns 
-- both the history of visited state and the actions taken to get there.
simulatePolicy :: (RandomGen g, MDP p s a,Policy p' s a) =>
                   g -> p s a -> p' s a -> s -> Int -> ([(a,s)],g)
simulatePolicy rGen mdp pol state eps = (tail $ fst r,snd r)
    where r = sp rGen mdp pol [(undefined,state)] eps -- hack using undefined
                                                      -- here
          sp :: (RandomGen g,MDP p s a,Policy p' s a) =>
                 g -> p s a -> p' s a -> [(a,s)] -> Int -> ([(a,s)],g)
          sp rGen_ mdp_ policy hist eps_
              | eps_ <= 0  = (hist,newGen)
              | otherwise = sp newGen mdp_ policy (hist ++ [(a,s1)]) (eps_-1)
              where s0 = snd $ last hist -- starting state
                    (s1,newGen) = simulateStep rGen_ mdp_ a s0
                    a = action policy s0 -- action for current state

-- Same as simulatePolicy, except that it does not return the new number
-- generator, only the transition history.
simulatePolicy_ :: (RandomGen g, MDP p s a,Policy p' s a) =>
                  g -> p s a -> p' s a -> s -> Int -> [(a,s)]
simulatePolicy_ g m p s e = fst $ simulatePolicy g m p s e

-- Simulate a policy using the given random number simulator, the policy, the
-- starting state and the required number of episodes. The function returns 
-- both the history of visited state and the actions taken to get there.
simulateRandomPolicy :: (RandomGen g, MDP p s a,RandomPolicy p' s a) =>
                   g -> p s a -> p' s a -> s -> Int -> ([(a,s)],g)
simulateRandomPolicy rGen mdp pol state eps = (tail $ fst r,snd r)
    where r = sp rGen mdp pol [(undefined,state)] eps -- hack using undefined
                                                      -- here
          sp :: (RandomGen g,MDP p s a,RandomPolicy p' s a) =>
                 g -> p s a -> p' s a -> [(a,s)] -> Int -> ([(a,s)],g)
          sp rGen_ mdp_ policy hist eps_
              | eps_ <= 0  = (hist,newGen)
              | otherwise = sp newGen mdp_ policy (hist ++ [(a,s1)]) (eps_-1)
              where s0 = snd $ last hist -- starting state
                    (s1,newGen) = simulateStep simGen mdp_ a s0
                    (simGen,policyGen) = split rGen_
                    a = randomAction policy policyGen s0 -- action for current
                                                         -- state

-- Same as simulateRandomPolicy, except that it does not return the new number
-- generator, only the transition history.
simulateRandomPolicy_ :: (RandomGen g, MDP p s a,RandomPolicy p' s a) =>
                  g -> p s a -> p' s a -> s -> Int -> [(a,s)]
simulateRandomPolicy_ g m p s e = fst $ simulateRandomPolicy g m p s e
