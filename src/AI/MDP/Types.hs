{-
   File	       : $1
   Module      : AI.MDP.Types
   Copyright   : Copyright (C) 2012 Oliver Stollmann
   License     : New BSD License (3-clause)

   Maintainer  : Oliver Stollmann <oliver@stollmann.net>
   Stability   : alpha
   Portability : portable

   <Module documentation>
-}

{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}

module AI.MDP.Types where

import qualified Data.List as L
import System.Random

import AI.MDP.Util

type Prob  = Double
type Rwrd  = Double
type Value = Double

--------------------------------------------------------------------------------
-- Type classes

class MDP p s a where

    states  :: p s a -> [s]
    actions :: p s a -> [a]

    stateName  :: p s a -> s -> String
    actionName :: p s a -> a -> String

    -- Return a transition probability (note: should throw an error when out of
    -- bounds)
    tp  :: p s a -> a -> s -> s -> Prob
    
    -- Return a reward (note: should throw an error when out of bounds)
    rw  :: p s a -> a -> s -> s -> Rwrd

class MDP p s a => UpdatableMDP p s a where

    -- Update a transition probability
    updateTP :: p s a -> a -> s -> s -> Prob -> p s a
    
    -- Update a reward
    updateRW :: p s a -> a -> s -> s -> Rwrd -> p s a

class UpdatableMDP p s a => BuildableMDP p s a where
    
    empty :: p s a

    setStates  :: p s a -> [s] -> p s a
    setActions :: p s a -> [a] -> p s a

    setStateNames  :: p s a -> [String] -> p s a
    setActionNames :: p s a -> [String] -> p s a
    

class Policy p s a where
    action :: p s a -> s -> a

class RandomPolicy p s a where
    randomAction :: (RandomGen g) => p s a -> g -> s -> a

--------------------------------------------------------------------------------
-- Helper functions from MDP type class

-- Return a list of state names
stateNames  :: MDP p s a => p s a -> [String]
stateNames  p = [stateName  p s | s <- states  p]

-- Return a list of action names
actionNames :: MDP p s a => p s a -> [String]
actionNames p = [actionName p a | a <- actions p]

-- Return the index of a state
stateIx  :: (Eq s,MDP p s a) => p s a -> s -> Maybe Int
stateIx p s  = L.findIndex ((==) s) (states p)

-- Return the index of an action
actionIx :: (Eq a,MDP p s a) => p s a -> a -> Maybe Int
actionIx p a = L.findIndex ((==) a) (actions p)

-- Return the index of a state given a name
stateIxFromName  :: MDP p s a => p s a -> String -> Maybe Int
stateIxFromName p s  = L.findIndex ((==) s) (stateNames p)

-- Return the index of an action given a name
actionIxFromName :: MDP p s a => p s a -> String -> Maybe Int
actionIxFromName p a = L.findIndex ((==) a) (actionNames p)

-- Uncurried version of tp
tp' :: MDP p s a => p s a -> (a,s,s)          -> Prob
tp' p (a,s,s') = tp p a s s'

-- Uncurried version of rw
rw' :: MDP p s a => p s a -> (a,s,s) -> Rwrd
rw' p (a,s,s') = rw p a s s'

-- Return a transition probability vector given an action and a source state
tpAS :: MDP p s a => p s a -> a -> s -> [Prob]
tpAS p a s = [tp p a s s' | s' <- states  p]

-- Return a transition probability matrix given an action
tpA :: MDP p s a => p s a -> a -> [[Prob]]
tpA p a = [tpAS p a s | s <- states p]

-- Return the entire transition probability cube
tps :: MDP p s a => p s a -> [[[Prob]]]
tps p = [tpA p a | a <- actions p]

-- Return a reward vector given an action and a source state
rwAS :: MDP p s a => p s a -> a -> s -> [Rwrd]
rwAS p a s = [rw p a s s' | s' <- states p]

-- Return a reward matrix given an action
rwA :: MDP p s a => p s a -> a -> [[Rwrd]]
rwA p a = [rwAS p a s | s <- states p]

-- Return the entire reward cube
rws :: MDP p s a => p s a -> [[[Rwrd]]]
rws p = [rwA p a | a <- actions p]

--------------------------------------------------------------------------------
-- Helper functions from UpdatableMDP instances 

-- Curried version of updateTP
updateTP' :: UpdatableMDP p s a => p s a -> (a,s,s) -> Prob -> p s a
updateTP' p (a,s,s') = updateTP p a s s'

-- Curried version of updateRW
updateRW' :: UpdatableMDP p s a => p s a -> (a,s,s) -> Rwrd -> p s a
updateRW' p (a,s,s') = updateRW p a s s'

-- Update all transition probabilities using a transition probability cube
-- Note: this type signature requires FlexibleContexts (otherwise must be left
--       out)
updateTPs :: UpdatableMDP p Int Int => p Int Int -> [[[Prob]]] -> p Int Int
updateTPs p prs = foldl update p us
    where us = [(a,s,s', prs !!!! (a,s,s')) | a  <- actions p
                                             ,s  <- states  p
                                             ,s' <- states  p]
          update p_ (a,s,s',pr_) = updateTP p_ a s s' pr_

-- Update all rewards using a reward cube 
-- Note: this type signature requires FlexibleContexts (otherwise must be left
--       out)
updateRWs :: UpdatableMDP p Int Int => p Int Int -> [[[Rwrd]]] -> p Int Int
updateRWs p rws_ = foldl update p us
    where us = [(a,s,s',rws_ !!!! (a,s,s')) | a  <- actions p
                                             ,s  <- states  p
                                             ,s' <- states  p]
          update p_ (a,s,s',rw_) = updateRW p_ a s s' rw_

