{-
   File	       : $1
   Module      : AI.MDP.Reader
   Copyright   : Copyright (C) 2012 Oliver Stollmann
   License     : New BSD License (3-clause)

   Maintainer  : Oliver Stollmann <oliver@stollmann.net>
   Stability   : alpha
   Portability : portable

   <Module documentation>
-}

{-# LANGUAGE FlexibleContexts #-}

module AI.MDP.Reader (
    fromJSON
) where

import Prelude hiding((<))
import AI.MDP.Types

import Text.JSON


fromJSON :: BuildableMDP p Int Int => String -> Either String (p Int Int)
fromJSON str = case decodeStrict str of
    Error err -> Left err
    Ok   json -> case fromJSON' json of
                    Error err -> Left  err
                    Ok  pomdp -> Right pomdp

fromJSON' :: BuildableMDP p Int Int => JSObject JSValue -> Result (p Int Int)
fromJSON' obj = do
           stateNames'  <- (<) "state_names"       :: Result [String]
           actionNames' <- (<) "action_names"      :: Result [String]
           tps'         <- (<) "tps"               :: Result [[[Prob]]]
           rws'         <- (<) "rws"               :: Result [[[Rwrd]]]
           let states'  = [0..(length  stateNames')-1]
           let actions' = [0..(length actionNames')-1]
           let pomdp = compose [flip setStates  states'
                               ,flip setActions actions'
                               ,flip setStateNames  stateNames'
                               ,flip setActionNames actionNames'
                               ,flip updateTPs tps'
                               ,flip updateRWs rws'] empty
           return pomdp
        where (<) :: JSON a => String -> Result a
              (<) = flip valFromObj obj
              compose :: [a -> a] -> a -> a
              compose fs v = foldl (flip (.)) id fs $ v
