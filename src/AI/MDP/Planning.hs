{-
   File	       : $1
   Module      : AI.MDP.Planning
   Copyright   : Copyright (C) 2012 Oliver Stollmann
   License     : New BSD License (3-clause)

   Maintainer  : Oliver Stollmann <oliver@stollmann.net>
   Stability   : alpha
   Portability : portable

   Planning of MDPs.
-}

module AI.MDP.Planning where

import AI.MDP.Types

import Data.List
import Data.Maybe

type Discount = Double
type Epsilon  = Double

-- Get infinity
infinity :: Double
infinity = 1/0 :: Double

