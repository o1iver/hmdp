{-
   File	       : $1
   Module      : AI.MDP.Impl
   Copyright   : Copyright (C) 2012 Oliver Stollmann
   License     : New BSD License (3-clause)

   Maintainer  : Oliver Stollmann <oliver@stollmann.net>
   Stability   : alpha
   Portability : portable

-}

{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}

module AI.MDP.Impl (
     MM
    ,x1MM
    ,x2MM
)where

import qualified Data.Map as M
import qualified Data.List as L
import Control.Monad()

import AI.MDP.Types

--------------------------------------------------------------------------------
-- Map-based MDP implementation
--
-- Data.Map based MDP. Since TPs and RWs are usually sparse using a map
-- may be more efficient than using Array (array) or Matrix (hmatrix). Also the
-- map is implemented using a binary tree, meaning that lookups and updates, etc
-- are O(log n) which is ok.
--
-- Note: there is no point in saving probabilities or rewards equal to 0.0 as
-- tp returns 0.0 when a valid, in terms of index bounds, but non-existent key
-- is used to lookup values.

data MM s a = MM {
     mmStates  :: [s]
    ,mmActions :: [a]
    ,mmStateNames  :: Maybe [String]
    ,mmActionNames :: Maybe [String]
    ,mmTPs     :: M.Map (a,s,s) Prob
    ,mmRWs     :: M.Map (a,s,s) Rwrd
    } deriving (Show)


mmTP :: (Show s,Show a,Ord s,Ord a) => MM s a -> a -> s -> s -> Prob
mmTP p a s s'
    | s  `notElem` mmStates  p = error $ "invalid state index `"  ++show s ++ "'"
    | s' `notElem` mmStates  p = error $ "invalid state index `"  ++show s'++ "'"
    | a  `notElem` mmActions p = error $ "invalid action index `" ++show a ++ "'"
    | otherwise = case M.lookup (a,s,s') (mmTPs p) of
        Just pr -> pr
        Nothing -> 0.0

mmRW :: (Show s,Show a,Ord s,Ord a) => MM s a -> a -> s -> s -> Rwrd
mmRW p a s s'
    | s  `notElem` mmStates  p = error $ "invalid state index `"  ++show s ++ "'"
    | s' `notElem` mmStates  p = error $ "invalid state index `"  ++show s'++ "'"
    | a  `notElem` mmActions p = error $ "invalid action index `" ++show a ++ "'"
    | otherwise = case M.lookup (a,s,s') (mmRWs p) of
        Just _rw -> _rw
        Nothing -> 0.0

mmStateName :: (Eq s,Show s) => MM s a -> s -> String
mmStateName p s = names !! index
    where index = case L.findIndex (\s' -> s == s') $ mmStates p of
                    Just ix -> ix
                    Nothing -> error $ "AI.MDP.Types.MPstateName: invalid state `"++show s++"'"
          names = case mmStateNames p of
                    Just ns -> ns
                    Nothing -> ["state" ++ show _s | _s <- mmStates p]

mmActionName :: (Eq a,Show a) => MM s a -> a -> String
mmActionName p a = names !! index
    where index = case L.findIndex (\a' -> a == a') $ mmActions p of
                    Just ix -> ix
                    Nothing -> error $ "AI.MDP.Types.MPactionName: invalid action `"++show a++"'"
          names = case mmActionNames p of
                    Just ns -> ns
                    Nothing -> ["action" ++ show _a | _a <- mmActions p]

-- Instance AI.MDP.Types.MDP MP
instance (Eq   s,Eq   a
         ,Show s,Show a
         ,Ord  s,Ord  a) => MDP MM s a where

    states  = mmStates
    actions = mmActions

    stateName  = mmStateName
    actionName = mmActionName

    tp = mmTP
    rw = mmRW

mmUpdateTP :: (Ord s,Ord a) => MM s a -> a -> s -> s -> Prob -> MM s a
mmUpdateTP p a s s' t = p { mmTPs = newTPs }
    where newTPs = M.alter (\_ -> Just t) (a,s,s') oldTPs
          oldTPs = mmTPs p

mmUpdateRW :: (Ord s,Ord a) => MM s a -> a -> s -> s -> Rwrd -> MM s a
mmUpdateRW p a s s' t = p { mmRWs = newRWs }
    where newRWs = M.alter (\_ -> Just t) (a,s,s') oldRWs
          oldRWs = mmRWs p

-- Instance AI.MDP.Types.UpdatableMDP MM
instance (Eq   s,Eq   a
         ,Show s,Show a
         ,Ord  s,Ord  a) => UpdatableMDP MM s a where
    
    updateTP = mmUpdateTP
    updateRW = mmUpdateRW

mmSetStates  :: MM s a -> [s] -> MM s a
mmSetStates  p ss = p {mmStates  = ss}

mmSetActions :: MM s a -> [a] -> MM s a
mmSetActions p as = p {mmActions = as}

mmSetStateNames :: MM s a -> [String] -> MM s a
mmSetStateNames  p sn = p {mmStateNames  = Just sn}

mmSetActionNames :: MM s a -> [String] -> MM s a
mmSetActionNames p an = p {mmActionNames = Just an}

mmEmpty :: (Ord a,Ord s) => MM a s
mmEmpty = MM {mmStates  = []
             ,mmActions = []
             ,mmStateNames  = Nothing
             ,mmActionNames = Nothing
             ,mmTPs = M.empty
             ,mmRWs = M.empty}

-- Instance AI.MDP.Types.BuildableMDP MM
instance (Eq   s,Eq   a
         ,Show s,Show a
         ,Ord  s,Ord a) => BuildableMDP MM s a where
    
    empty = mmEmpty

    setStates  = mmSetStates
    setActions = mmSetActions

    setStateNames  = mmSetStateNames
    setActionNames = mmSetActionNames

--------------------------------------------------------------------------------
-- Map-based Policy implementation

data SimplePolicy s a = SimplePolicy {
    simplePolicy :: M.Map s a
    } deriving (Show)

simplePolicyAction :: (Ord s) => SimplePolicy s a -> s -> a
simplePolicyAction p s = case M.lookup s (simplePolicy p) of
    Just a -> a
    Nothing -> error "AI.MDP.Types: no action for given state"

instance (Ord s) => Policy SimplePolicy s a where
    action = simplePolicyAction

--------------------------------------------------------------------------------
-- Example: MM
x1mmStates  :: [Int]
x1mmStates  = [0,1]
x1mmActions :: [Int]
x1mmActions = [0,1]
x1mmTPs :: M.Map (Int,Int,Int) Prob
x1mmTPs     = M.fromList [((0,0,0),0.1),((0,0,1),0.9)
                         ,((0,1,0),0.5),((0,1,1),0.5)
                         ,((1,0,0),1.0),((1,0,1),0.0)
                         ,((1,1,0),0.2),((1,1,1),0.8)]
                            
x1mmRWs :: M.Map (Int,Int,Int) Rwrd
x1mmRWs     = M.fromList [((a,s,s'),2)|a <- as,s <- ss,s' <- ss]
    where ss = x1mmStates
          as = x1mmActions

x1MM :: MM Int Int
x1MM = MM {
     mmStates  = x1mmStates
    ,mmActions = x1mmActions

    ,mmStateNames  = Nothing
    ,mmActionNames = Nothing

    ,mmTPs = x1mmTPs
    ,mmRWs = x1mmRWs
    }

x2MM :: MM Int Int
x2MM = x1MM {
     mmStateNames  = Just ["left","right"]
    ,mmActionNames = Just ["forward","backward"]
    }

