SHELL := /bin/bash

BASEDIR=$(shell pwd)
PROJECT=$(shell basename $(BASEDIR))
VERSION=$(shell cat VERSION)
# Feel free to rewrite this (way too ugly)
AUTHORS=$(shell cat AUTHORS | grep - | cut -d ' ' -f 2- | tr '\n' ',' | sed 's/,/, /g' | sed 's/, \?$$//')
ARCH=$(shell uname -p)
BINNAME="$(PROJECT)-$(ARCH)"

DISTPKG="$(PROJECT)-$(VERSION).tar.gz"

xx: 
	@echo "Cleaning..."
	cabal clean
	@echo "Configuring..."
	cabal configure
	@echo "Building..."
	cabal build
	@echo "Installing..."
	cabal install
build:
	@echo "Cleaning..."
	cabal clean
	@echo "Configuring..."
	cabal configure
	@echo "Building..."
	cabal build

install:
	@echo "Installing..."
	cabal install

info:
	@echo "Project: $(PROJECT)"
	@echo "Version: $(VERSION)"
	@echo "Authors: $(AUTHORS)"
	@echo "Basedir: $(BASEDIR)"
	@echo "Arch:    $(ARCH)"
	@echo "Output:  $(BINNAME)"
	@echo "Distpkg: $(DISTPKG)"
project:
	@echo "$(PROJECT)"
version:
	@echo "$(VERSION)"
authors:
	@echo "$(AUTHORS)"
dist:
	@echo "Creating distribution package..."
	tar -czf $(DISTPKG) *
pkg:
	@echo "Creating distribution package..."
	tar -czf $(DISTPKG) *
clean:
	cabal clean
	@echo "Removing dist package..."
	rm -f $(DISTPKG)
